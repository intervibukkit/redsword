# RedSword

Add customizable flying swords, like a Murder Mystery game. Have API.

* For java >= 1.8 and spigot >= 1.12.2

## How to use

Click empty slot in quick bar for get you sword. Drop sword for remove from inventory. Click right mouse button for throw or get sword.

## Commands

### User

* **/reds random** - give random sword to you
* **/reds unowner** - remove you sword

### Moder

* **/reds give player sword** - give sword
* **/reds take player** - take sword
* **/reds list** - see list sword keys
* **/reds owners** - see list online owners
* **/reds total** - see owners list size

### Admin

* **/reds reload** - reload config and users
* **/reds save** - save users
* **/reds clear** - clear all maps, death flying swords

## Permissions

* **redsword.sword** - for using swords
* **redsword.use** - for uning commands
* **redsword.unowner** - for uning unowner command
* **redsword.random** - for uning random command
* **redsword.exempt.world** - for uning in any worlds
* **redsword.exempt.click** - for not work anti-dupe
* **redsword.moder.give** - for uning give command
* **redsword.moder.take** - for uning take command
* **redsword.moder.list** - for uning list command
* **redsword.moder.owners** - for uning owners command
* **redsword.moder.total** - for using total command
* **redsword.admin.reload** - for uning reload command
* **redsword.admin.save** - for uning save command
* **redsword.admin.clear** - for uning clear command
