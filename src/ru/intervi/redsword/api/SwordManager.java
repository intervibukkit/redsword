package ru.intervi.redsword.api;

import org.bukkit.Bukkit;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.intervi.redsword.Config;
import ru.intervi.redsword.FlyBrain;
import ru.intervi.redsword.api.sword.SwordData;

import java.util.*;

/**
 * For use in you plugin. All methods synchronized for threads safety.
 */
public class SwordManager {
    private final Config CONFIG;
    private final FlyBrain BRAIN;
    private static SwordManager instance;

    public SwordManager(Config config, FlyBrain flyBrain) {
        CONFIG = config;
        BRAIN = flyBrain;
        instance = this;
    }

    /**
     * get current instance
     * @return current object
     * @throws ApiNotEnabledException if API disabled in config, check isApiEnabled
     */
    public static SwordManager getInstance() throws ApiNotEnabledException {
        if (instance != null) {
            if (!instance.CONFIG.apiEnabled) {
                throw new ApiNotEnabledException();
            }
        }
        return instance;
    }

    /**
     * check player has owned any sword
     * @param uuid Player UUID
     * @return true if yes
     */
    public boolean isOwner(UUID uuid) {
        synchronized (CONFIG.USERS) {
            return CONFIG.USERS.containsKey(uuid);
        }
    }

    /**
     * check exists sword key
     * @param key unique key from config (name section)
     * @return true if yes
     */
    public boolean isSword(String key) {
        synchronized (CONFIG.SWORDS) {
            return CONFIG.SWORDS.containsKey(key);
        }
    }

    /**
     * get sword key from Player (owner) UUID
     * @param uuid Player UUID
     * @return unique key from config
     * @exception NoSuchElementException if UUID not owner
     */
    public String getSwordKey(UUID uuid) {
        if (!isOwner(uuid)) {
            throw new NoSuchElementException(uuid.toString() + " not owner any sword");
        }
        synchronized (CONFIG.USERS) {
            return CONFIG.USERS.get(uuid);
        }
    }

    /**
     * set sword key for Player
     * @param uuid Player UUID
     * @param key unique key from config
     * @exception NoSuchElementException if key not found
     */
    public void setSword(UUID uuid, String key) {
        synchronized (CONFIG.SWORDS) {
            if (!CONFIG.SWORDS.containsKey(key)) {
                throw new NoSuchElementException(key + " not found");
            }
        }
        synchronized (CONFIG.USERS) {
            if (CONFIG.USERS.containsKey(uuid)) {
                CONFIG.USERS.replace(uuid, key);
            } else {
                CONFIG.USERS.put(uuid, key);
            }
        }
    }

    /**
     * get all swords
     * @return unmodifiable Map
     */
    public Map<String, SwordData> getSwords() {
        synchronized (CONFIG.SWORDS) {
            return Collections.unmodifiableMap(CONFIG.SWORDS);
        }
    }

    /**
     * get all owners
     * @return unmodifiable Map
     */
    public Map<UUID, String> getUsers() {
        synchronized (CONFIG.USERS) {
            return Collections.unmodifiableMap(CONFIG.USERS);
        }
    }

    /**
     * get sword data from key
     * @param key unique key from config (name section)
     * @return current data
     * @exception NoSuchElementException if key not found
     */
    public SwordData getSword(String key) {
        synchronized (CONFIG.SWORDS) {
            if (!CONFIG.SWORDS.containsKey(key)) {
                throw new NoSuchElementException(key + " not found");
            }
            return CONFIG.SWORDS.get(key);
        }
    }

    /**
     * add sword to config (not save, only RAM)
     * @param key unique key
     * @param sword current data
     * @exception IllegalArgumentException if key already exists
     */
    public void addSword(String key, SwordData sword) {
        synchronized (CONFIG.SWORDS) {
            if (CONFIG.SWORDS.containsKey(key)) {
                throw new IllegalArgumentException(key + " already exists");
            }
            CONFIG.SWORDS.put(key, sword);
        }
    }

    /**
     * remove SwordData from config (not save, only RAM)
     * @param key unique key
     * @exception NoSuchElementException if key not found
     */
    public void removeSword(String key) {
        synchronized (CONFIG.SWORDS) {
            if (!CONFIG.SWORDS.containsKey(key)) {
                throw new NoSuchElementException(key + " not found");
            }
            CONFIG.SWORDS.remove(key);
        }
    }

    /**
     * unset (remove) sword in player
     * @param uuid Player UUID
     * @exception NoSuchElementException if player not owner any sword
     */
    public void unsetSword(UUID uuid) {
        if (!isOwner(uuid)) {
            throw new NoSuchElementException(uuid.toString() + " not owner any sword");
        }
        synchronized (CONFIG.USERS) {
            CONFIG.USERS.remove(uuid);
        }
    }

    /**
     * get current ItemStack (sword) from key
     * @param key unique key
     * @return current ItemStack
     * @exception NoSuchElementException if key not found
     */
    public ItemStack getItemStack(String key) {
        SwordData data;
        synchronized (CONFIG.SWORDS) {
            if (!CONFIG.SWORDS.containsKey(key)) {
                throw new NoSuchElementException(key + " not found");
            }
            data = CONFIG.SWORDS.get(key);
        }
        ItemMeta meta = Bukkit.getItemFactory().getItemMeta(data.TYPE);
        meta.setLocalizedName(key);
        meta.setDisplayName(data.NAME);
        for (Map.Entry<Enchantment, Integer> entry : data.CHARS.entrySet()) {
            meta.addEnchant(entry.getKey(), entry.getValue(), true);
        }
        meta.setLore(data.LORE);
        meta.setUnbreakable(true);
        ItemStack item = new ItemStack(data.TYPE, 1);
        item.setItemMeta(meta);
        return item;
    }

    /**
     * check sword is flying
     * @param uuid Player UUID
     * @return true if yes
     */
    public boolean isSwordFlying(UUID uuid) {
        return BRAIN.containsPlayer(uuid);
    }

    /**
     * get Worlds black list (take sword from inventory before teleport, block use)
     * @return unmodifiable List with names
     */
    public List<String> getWorldsBlackList() {
        return Collections.unmodifiableList(CONFIG.noInWorlds);
    }

    /**
     * change plugin enable status (not save, only RAM)
     * @param arg new value
     */
    public void setPluginEnabled(boolean arg) {
        CONFIG.enabled = arg;
    }

    /**
     * check plugin enabled
     * @return true if yes
     */
    public boolean isPluginEnabled() {
        return CONFIG.enabled;
    }

    /**
     * check enabled API
     * @return true if yes
     */
    public static boolean isApiEnabled() {
        return instance.CONFIG.apiEnabled;
    }
}
