package ru.intervi.redsword.api;

public class ApiNotEnabledException extends Exception {
    public ApiNotEnabledException() {
        super("API not enabled in config");
    }
}
