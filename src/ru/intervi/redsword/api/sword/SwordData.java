package ru.intervi.redsword.api.sword;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;

import java.util.*;

public class SwordData {
    public final Map<Enchantment, Integer> CHARS;
    public final List<String> LORE;
    public final Material TYPE;
    public final String NAME;
    public final int PARTICLE_NUM;
    public final double PARTICLE_OFFSET;
    public final Particle TRACE_PARTICLE;
    public final double TRACE_INTERVAL;
    public final double DAMAGE_PLAYER;
    public final double DAMAGE_ENTITY;
    public final float EXPLOSION_POWER;
    public final int FLY_DISTANCE;
    public final double SPEED;
    public final int FIRE_TICKS_PLAYER;
    public final int FIRE_TICKS_ENTITY;

    public SwordData(ConfigurationSection section) {
        ConfigurationSection charSection = section.getConfigurationSection("chars");
        HashMap<Enchantment, Integer> charMap = new HashMap<>();
        for (String key : charSection.getKeys(false)) {
            charMap.put(Enchantment.getByName(key.toUpperCase()), charSection.getInt(key));
        }
        CHARS = Collections.unmodifiableMap(charMap);
        ArrayList<String> lore = new ArrayList<>();
        for (String line : section.getStringList("lore")) {
            lore.add(ChatColor.translateAlternateColorCodes('&', line));
        }
        LORE = Collections.unmodifiableList(lore);
        TYPE = Material.valueOf(section.getString("type").toUpperCase());
        NAME = ChatColor.translateAlternateColorCodes('&', section.getString("name"));
        PARTICLE_NUM = section.getInt("particlesNum");
        PARTICLE_OFFSET = section.getDouble("particleOffset");
        TRACE_PARTICLE = Particle.valueOf(section.getString("traceParticle").toUpperCase());
        TRACE_INTERVAL = section.getDouble("traceInt");
        DAMAGE_PLAYER = section.getDouble("damagePlayer");
        DAMAGE_ENTITY = section.getDouble("damageEntity");
        EXPLOSION_POWER = Double.valueOf(section.getDouble("explosionPower")).floatValue();
        FLY_DISTANCE = section.getInt("flyDistance");
        SPEED = section.getDouble("speed");
        FIRE_TICKS_PLAYER = section.getInt("playerFireTicks");
        FIRE_TICKS_ENTITY = section.getInt("entityFireTicks");
    }
}
