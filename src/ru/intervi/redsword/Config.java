package ru.intervi.redsword;

import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import ru.intervi.redsword.api.sword.SwordData;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Config implements Runnable {
    private final Main MAIN;
    public boolean enabled;
    public boolean apiEnabled;
    int saveInt;
    public final Map<String, SwordData> SWORDS = Collections.synchronizedMap(new HashMap<>());
    public final Map<UUID, String> USERS = Collections.synchronizedMap(new HashMap<>());
    public List<String> noInWorlds;

    String noperm;
    List<String> help;
    String reloaded;
    String saved;
    String nosword;
    String noplayer;
    String alreadyOwner;
    String given;
    String noOwner;
    String removed;
    String randomed;
    String onlyPlayer;
    String listTop;
    String ownersTop;
    String cleared;
    String youOwner;
    String youNoOwner;
    String unowner;
    String total;

    Config(Main main) {
        MAIN = main;
        load();
    }

    void load() {
        MAIN.saveDefaultConfig();
        MAIN.reloadConfig();
        FileConfiguration conf = MAIN.getConfig();
        enabled = conf.getBoolean("enabled");
        apiEnabled = conf.getBoolean("apiEnabled");
        noInWorlds = conf.getStringList("noInWorlds");
        saveInt = conf.getInt("saveInt");
        synchronized (SWORDS) {
            SWORDS.clear();
            ConfigurationSection swordSec = conf.getConfigurationSection("swords");
            for (String key : swordSec.getKeys(false)) {
                SWORDS.put(key, new SwordData(swordSec.getConfigurationSection(key)));
            }
        }
        File file = new File(MAIN.getDataFolder(), "users.yml");
        if (!file.isFile()) {
            try {
                file.createNewFile();
            }
            catch(IOException e) {
                e.printStackTrace();
            }
        }
        FileConfiguration db = YamlConfiguration.loadConfiguration(file);
        synchronized (USERS) {
            USERS.clear();
            for (String key : db.getKeys(false)) {
                USERS.put(UUID.fromString(key), db.getString(key));
            }
        }
        loadLocale(conf);
    }

    private String color(String str) {
        return ChatColor.translateAlternateColorCodes('&', str);
    }

    private void loadLocale(FileConfiguration conf) {
        noperm = color(conf.getString("noperm"));
        ArrayList<String> help = new ArrayList<>();
        for (String line : conf.getStringList("help")) {
            help.add(color(line));
        }
        this.help = help;
        reloaded = color(conf.getString("reloaded"));
        saved = color(conf.getString("saved"));
        nosword = color(conf.getString("nosword"));
        noplayer = color(conf.getString("noplayer"));
        alreadyOwner = color(conf.getString("alreadyOwner"));
        given = color(conf.getString("given"));
        noOwner = color(conf.getString("noOwner"));
        removed = color(conf.getString("removed"));
        randomed = color(conf.getString("randomed"));
        onlyPlayer = color(conf.getString("onlyPlayer"));
        listTop = color(conf.getString("listTop"));
        ownersTop = color(conf.getString("ownersTop"));
        cleared = color(conf.getString("cleared"));
        youOwner = color(conf.getString("youOwner"));
        youNoOwner = color(conf.getString("youNoOwner"));
        unowner = color(conf.getString("unowner"));
        total = color(conf.getString("total"));
    }

    void save() {
        File file = new File(MAIN.getDataFolder(), "users.yml");
        if (!file.isFile()) {
            try {
                file.createNewFile();
            }
            catch(IOException e) {
                e.printStackTrace();
            }
        }
        FileConfiguration db = YamlConfiguration.loadConfiguration(file);
        synchronized (USERS) {
            for (Map.Entry<UUID, String> entry : USERS.entrySet()) {
                db.set(entry.getKey().toString(), entry.getValue());
            }
        }
        try {
            db.save(file);
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        save();
    }
}
