package ru.intervi.redsword;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import ru.intervi.redsword.api.SwordManager;
import ru.intervi.redsword.api.sword.SwordData;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;

public class Main extends JavaPlugin {
    Config config = new Config(this);
    FlyBrain flyBrain = new FlyBrain(this);
    SwordManager manager = new SwordManager(config, flyBrain);
    private Events events = new Events(this);

    @Override
    public void onEnable() {
        flyBrain.startTimer();
        getServer().getPluginManager().registerEvents(events, this);
    }

    @Override
    public void onDisable() {
        config.save();
        flyBrain.stopTimer();
        flyBrain.clear();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("redsword.use") && !(sender instanceof ConsoleCommandSender)) {
            sender.sendMessage(config.noperm);
            return true;
        }
        if (args == null || args.length == 0) {
            for (String line : config.help) sender.sendMessage(line);
            return true;
        }
        Player player;
        switch (args[0].toLowerCase()) {
            case "reload":
                if (!sender.hasPermission("redsword.admin.reload") && !(sender instanceof ConsoleCommandSender)) {
                    sender.sendMessage(config.noperm);
                    return true;
                }
                config.load();
                sender.sendMessage(config.reloaded);
                break;
            case "save":
                if (!sender.hasPermission("redsword.admin.save") && !(sender instanceof ConsoleCommandSender)) {
                    sender.sendMessage(config.noperm);
                    return true;
                }
                config.save();
                sender.sendMessage(config.saved);
                break;
            case "give":
                if (!sender.hasPermission("redsword.moder.give") && !(sender instanceof ConsoleCommandSender)) {
                    sender.sendMessage(config.noperm);
                    return true;
                }
                if (args.length < 3) {
                    for (String line : config.help) sender.sendMessage(line);
                    break;
                }
                player = Bukkit.getPlayer(args[1]);
                if (player == null || !player.isOnline()) {
                    sender.sendMessage(config.noplayer);
                    break;
                }
                if (manager.isOwner(player.getUniqueId())) {
                    sender.sendMessage(config.alreadyOwner);
                    break;
                }
                if (!manager.isSword(args[2])) {
                    sender.sendMessage(config.nosword);
                    break;
                }
                manager.setSword(player.getUniqueId(), args[2]);
                player.getInventory().addItem(manager.getItemStack(args[2]));
                sender.sendMessage(config.given);
                break;
            case "take":
                if (!sender.hasPermission("redsword.moder.take") && !(sender instanceof ConsoleCommandSender)) {
                    sender.sendMessage(config.noperm);
                    return true;
                }
                if (args.length < 2) {
                    for (String line : config.help) sender.sendMessage(line);
                    break;
                }
                player = Bukkit.getPlayer(args[1]);
                if (player == null || !player.isOnline()) {
                    sender.sendMessage(config.noplayer);
                    break;
                }
                if (!manager.isOwner(player.getUniqueId())) {
                    sender.sendMessage(config.noOwner);
                    break;
                }
                player.getInventory().remove(manager.getItemStack(manager.getSwordKey(player.getUniqueId())));
                config.USERS.remove(player.getUniqueId());
                sender.sendMessage(config.removed);
                break;
            case "random":
                if (!sender.hasPermission("redsword.random") && !(sender instanceof ConsoleCommandSender)) {
                    sender.sendMessage(config.noperm);
                    return true;
                }
                if (!(sender instanceof Player)) {
                    sender.sendMessage(config.onlyPlayer);
                    break;
                }
                player = (Player) sender;
                if (manager.isOwner(player.getUniqueId())) {
                    sender.sendMessage(config.youOwner);
                    break;
                }
                int ind = new Random().nextInt(config.SWORDS.size());
                String key = new ArrayList<String>(config.SWORDS.keySet()).get(ind);
                manager.setSword(player.getUniqueId(), key);
                player.getInventory().addItem(manager.getItemStack(key));
                sender.sendMessage(config.randomed);
                break;
            case "list":
                if (!sender.hasPermission("redsword.moder.list") && !(sender instanceof ConsoleCommandSender)) {
                    sender.sendMessage(config.noperm);
                    return true;
                }
                ArrayList<String> swordList = new ArrayList<>();
                for (Map.Entry<String, SwordData> entry : config.SWORDS.entrySet()) {
                    swordList.add(entry.getKey() + ": " + entry.getValue().NAME);
                }
                sender.sendMessage(config.listTop);
                for (String line : swordList) sender.sendMessage(line);
                break;
            case "owners":
                if (!sender.hasPermission("redsword.moder.owners") && !(sender instanceof ConsoleCommandSender)) {
                    sender.sendMessage(config.noperm);
                    return true;
                }
                String owners = "";
                for (Player player1 : Bukkit.getOnlinePlayers()) {
                    if (manager.isOwner(player1.getUniqueId())) owners += player1.getDisplayName() + ", ";
                }
                if (!owners.isEmpty()) owners = owners.substring(0, owners.length() - 2);
                sender.sendMessage(config.ownersTop);
                sender.sendMessage(owners);
                break;
            case "clear":
                if (!sender.hasPermission("redsword.admin.clear") && !(sender instanceof ConsoleCommandSender)) {
                    sender.sendMessage(config.noperm);
                    return true;
                }
                flyBrain.clear();
                sender.sendMessage(config.cleared);
                break;
            case "unowner":
                if (!sender.hasPermission("redsword.unowner") && !(sender instanceof ConsoleCommandSender)) {
                    sender.sendMessage(config.noperm);
                    return true;
                }
                if (!(sender instanceof Player)) {
                    sender.sendMessage(config.onlyPlayer);
                    break;
                }
                player = (Player) sender;
                if (!manager.isOwner(player.getUniqueId())) {
                    sender.sendMessage(config.youNoOwner);
                    break;
                }
                player.getInventory().remove(manager.getItemStack(manager.getSwordKey(player.getUniqueId())));
                manager.unsetSword(player.getUniqueId());
                sender.sendMessage(config.unowner);
                break;
            case "total":
                if (!sender.hasPermission("redsword.moder.total") && !(sender instanceof ConsoleCommandSender)) {
                    sender.sendMessage(config.noperm);
                    return true;
                }
                sender.sendMessage(MessageFormat.format(config.total, String.valueOf(config.USERS.size())));
                break;
            default:
                return false;
        }
        return true;
    }
}
