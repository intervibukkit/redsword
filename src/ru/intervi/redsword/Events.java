package ru.intervi.redsword;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Iterator;
import java.util.UUID;

class Events implements Listener {
    private final Main MAIN;

    Events(Main main) {
        MAIN = main;
    }

    private boolean isSword(ItemStack item) {
        if (item == null) return false;
        if (!item.hasItemMeta()) return false;
        ItemMeta meta = item.getItemMeta();
        if (!meta.hasLocalizedName()) return false;
        synchronized (MAIN.config.SWORDS) {
            return MAIN.config.SWORDS.containsKey(meta.getLocalizedName());
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onWorldChange(PlayerChangedWorldEvent event) {
        if (!MAIN.config.enabled) return;
        if (!event.getPlayer().hasPermission("redsword.sword")) return;
        if (event.getPlayer().hasPermission("redsword.exempt.world")) return;
        if (!MAIN.config.noInWorlds.contains(event.getPlayer().getWorld().getName())) return;
        PlayerInventory inv = event.getPlayer().getInventory();
        for (ItemStack item : inv.getContents()) {
            if (!isSword(item)) continue;
            item.setItemMeta(null);
            item.setType(Material.AIR);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onDrop(PlayerDropItemEvent event) {
        if (!MAIN.config.enabled) return;
        if (!event.getPlayer().hasPermission("redsword.sword")) return;
        if (!isSword(event.getItemDrop().getItemStack())) return;
        event.getItemDrop().setItemStack(new ItemStack(Material.AIR));
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onDeath(PlayerDeathEvent event) {
        if (!MAIN.config.enabled) return;
        if (!event.getEntity().hasPermission("redsword.sword")) return;
        Iterator<ItemStack> iter = event.getDrops().iterator();
        while (iter.hasNext()) {
            if (!isSword(iter.next())) continue;
            iter.remove();
        }
    }

    private void antiDupeClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        if (player.hasPermission("redsword.exempt.click")) return;
        if (!event.getInventory().getType().equals(InventoryType.PLAYER) && isSword(event.getCursor())) {
            event.setCancelled(true);
            return;
        }
        if (player.hasPermission("redsword.sword") && MAIN.manager.isOwner(player.getUniqueId())) return;
        if (isSword(event.getCurrentItem())) {
            event.setCancelled(true);
            event.setCurrentItem(new ItemStack(Material.AIR));
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onClick(InventoryClickEvent event) {
        if (!MAIN.config.enabled) return;
        antiDupeClick(event);
        if (!event.getWhoClicked().hasPermission("redsword.sword")) return;
        if (!event.isRightClick()) return;
        if (!(event.getClickedInventory().getType().equals(InventoryType.PLAYER))) return;
        if (!event.getSlotType().equals(InventoryType.SlotType.QUICKBAR)) return;
        UUID uuid = event.getWhoClicked().getUniqueId();
        if (!MAIN.manager.isOwner(uuid)) return;
        if (event.getCurrentItem() != null && !event.getCurrentItem().getType().equals(Material.AIR)) return;
        if (event.getCursor() != null && !event.getCursor().getType().equals(Material.AIR)) return;
        for (ItemStack item : event.getWhoClicked().getInventory().getContents()) {
            if (isSword(item)) return;
        }
        event.getWhoClicked().getInventory().setItem(
                event.getSlot(), MAIN.manager.getItemStack(MAIN.manager.getSwordKey(uuid)))
        ;
        event.setCancelled(true);
        MAIN.flyBrain.remove(uuid);
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onDamage(EntityDamageEvent event) {
        if (!MAIN.config.enabled) return;
        if (!event.getEntity().getType().equals(EntityType.WITHER_SKELETON)) return;
        if (!MAIN.flyBrain.containsEntity(event.getEntity().getUniqueId())) return;
        event.getEntity().setFireTicks(0);
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onInteract(PlayerInteractEvent event) {
        if (!MAIN.config.enabled) return;
        if (!event.getAction().equals(Action.RIGHT_CLICK_AIR)) return;
        if (!event.getPlayer().hasPermission("redsword.sword")) return;
        UUID uuid = event.getPlayer().getUniqueId();
        if (!MAIN.manager.isOwner(uuid)) return;
        if (MAIN.flyBrain.containsPlayer(uuid)) return;
        if (!isSword(event.getPlayer().getInventory().getItemInMainHand())) return;
        MAIN.flyBrain.startFly(event.getPlayer(), MAIN.manager.getSwordKey(uuid));
        event.getPlayer().getInventory().setItemInMainHand(new ItemStack(Material.AIR));
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onTarget(EntityTargetEvent event) {
        if (!MAIN.config.enabled) return;
        if (!event.getEntityType().equals(EntityType.WITHER_SKELETON)) return;
        if (!MAIN.flyBrain.containsEntity(event.getEntity().getUniqueId())) return;
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onQuit(PlayerQuitEvent event) {
        if (!MAIN.config.enabled) return;
        MAIN.flyBrain.remove(event.getPlayer().getUniqueId());
    }
}
