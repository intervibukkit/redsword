package ru.intervi.redsword;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.scheduler.BukkitRunnable;
import ru.intervi.redsword.api.sword.SwordData;

import java.util.*;

class WorkerTask extends BukkitRunnable {
    private final FlyBrain BRAIN;
    private long oldTime = System.currentTimeMillis();

    WorkerTask(FlyBrain brain) {
        BRAIN = brain;
    }

    private static double round(double number, int scale) { //from mutagen in cyberforum
        int pow = 10;
        for (int i = 1; i < scale; i++)
            pow *= 10;
        double tmp = number * pow;
        return (double) (int) ((tmp - (int) tmp) >= 0.5f ? tmp + 1 : tmp) / pow;
    }

    private void explosion(SwordData sword, Location loc) {
        if (sword.EXPLOSION_POWER <= 0) return;
        double x = loc.getX();
        double y = loc.getY();
        double z = loc.getZ();
        loc.getWorld().createExplosion(x, y, z, sword.EXPLOSION_POWER, false, false);
    }

    private void trace(Location loc, Location startLoc, SwordData sword, Entity flyEntity) {
        if (round(flyEntity.getLocation().distance(startLoc), 1) / sword.TRACE_INTERVAL % 1 == 0) {
            loc.getWorld().spawnParticle(
                    sword.TRACE_PARTICLE,
                    flyEntity.getLocation(),
                    sword.PARTICLE_NUM,
                    sword.PARTICLE_OFFSET,
                    sword.PARTICLE_OFFSET,
                    sword.PARTICLE_OFFSET
            );
        }
    }

    @SuppressWarnings( "deprecation" )
    private boolean workMove(Location loc, FlyBrain.EntityData data, SwordData sword, UUID playerUuid, UUID flyEntityUuid) {
        if (loc.distance(data.oldLocation) < 1) return false;
        Player player = Bukkit.getPlayer(playerUuid);
        Collection<Entity> nearby = loc.getWorld().getNearbyEntities(loc, 1, 1, 1);
        boolean end = false;
        for (Entity entity : nearby) {
            if (!(entity instanceof Damageable)) continue;
            if (entity.getUniqueId().equals(playerUuid)) continue;
            if (entity.getUniqueId().equals(flyEntityUuid)) continue;
            boolean isPlayer = entity.getType().equals(EntityType.PLAYER);
            EntityDamageByEntityEvent event = new EntityDamageByEntityEvent( //all constructors is deprecated
                    player, entity, EntityDamageEvent.DamageCause.CUSTOM,
                    isPlayer ? sword.DAMAGE_PLAYER : sword.DAMAGE_ENTITY
            );
            BRAIN.MAIN.getServer().getPluginManager().callEvent(event);
            if (event.isCancelled()) continue;
            if (isPlayer) {
                ((Damageable) entity).damage(event.getDamage(), Bukkit.getEntity(playerUuid));
                entity.setFireTicks(sword.FIRE_TICKS_PLAYER);
            } else {
                ((Damageable) entity).damage(event.getDamage(), Bukkit.getEntity(playerUuid));
                entity.setFireTicks(sword.FIRE_TICKS_ENTITY);
            }
            end = true;
        }
        if (loc.distance(data.startLocation) >= sword.FLY_DISTANCE) {
            end = true;
        }
        data.oldLocation = loc;
        return end;
    }

    private boolean workDistance(Location loc, FlyBrain.EntityData data) {
        double distance = loc.distance(data.END_LOCATION);
        if (distance >= data.oldDistance) return true;
        data.oldDistance = distance;
        return false;
    }

    @Override
    public void run() {
        long newTime = System.currentTimeMillis();
        if ((newTime - oldTime) / 1000 >= BRAIN.MAIN.config.saveInt) {
            oldTime = newTime;
            new Thread(BRAIN.MAIN.config).start();
        }
        Iterator<Map.Entry<UUID, FlyBrain.EntityData>> iter = BRAIN.getAllData().iterator();
        while (iter.hasNext()) {
            Map.Entry<UUID, FlyBrain.EntityData> entry = iter.next();
            FlyBrain.EntityData data = entry.getValue();
            Entity flyEntity = data.ENTITY;
            flyEntity.setVelocity(data.VECTOR);
            if (!flyEntity.isValid()) {
                iter.remove();
                BRAIN.removeFromUsermap(data.STAND_UUID);
                continue;
            }
            Location loc = flyEntity.getLocation().getBlock().getLocation();
            SwordData sword = BRAIN.MAIN.manager.getSword(BRAIN.MAIN.manager.getSwordKey(entry.getKey()));
            if (workMove(loc, data, sword, entry.getKey(), data.STAND_UUID) || workDistance(flyEntity.getLocation(), data)) {
                flyEntity.remove();
                explosion(sword, loc);
                BRAIN.removeFromUsermap(data.STAND_UUID);
                iter.remove();
                Bukkit.getPlayer(entry.getKey()).getInventory().addItem(BRAIN.MAIN.manager.getItemStack(data.KEY));
                continue;
            }
            trace(loc, data.startLocation, sword, flyEntity);
        }
    }
}
