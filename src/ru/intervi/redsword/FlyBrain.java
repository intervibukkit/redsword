package ru.intervi.redsword;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.WitherSkeleton;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;
import ru.intervi.redsword.api.sword.SwordData;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class FlyBrain {
    final Main MAIN;
    private WorkerTask task;
    private HashMap<UUID, EntityData> stands = new HashMap<>();
    private HashMap<UUID, UUID> usermap = new HashMap<>();

    FlyBrain(Main main) {
        MAIN = main;
    }

    class EntityData {
        final UUID STAND_UUID;
        final Entity ENTITY;
        final String KEY;
        final Location END_LOCATION;
        final Vector VECTOR;
        Location oldLocation;
        Location startLocation;
        double oldDistance;

        private EntityData(Entity entity, String key, Location endLocation, Vector vector, double distance) {
            ENTITY = entity;
            STAND_UUID = entity.getUniqueId();
            KEY = key;
            END_LOCATION = endLocation;
            VECTOR = vector;
            oldLocation = entity.getLocation().clone();
            startLocation = entity.getLocation().clone();
            oldDistance = distance;
        }
    }

    void startTimer() {
        task = new WorkerTask(this);
        task.runTaskTimer(MAIN, 1, 1);
    }

    void stopTimer() {
        if (task == null) return;
        task.cancel();
        task = null;
    }

    void remove(UUID uuid) {
        if (!stands.containsKey(uuid)) return;
        stands.get(uuid).ENTITY.remove();
        usermap.remove(stands.get(uuid).STAND_UUID);
        stands.remove(uuid);
    }

    void removeFromUsermap(UUID uuid) {
        usermap.remove(uuid);
    }

    Set<Map.Entry<UUID, EntityData>> getAllData() {
        return stands.entrySet();
    }

    UUID getPlayerUuid(UUID uuid) {
        return usermap.get(uuid);
    }

    boolean containsEntity(UUID uuid) {
        return usermap.containsKey(uuid);
    }

    public boolean containsPlayer(UUID uuid) {
        return stands.containsKey(uuid);
    }

    void clear() {
        stands.clear();
        usermap.clear();
    }

    void startFly(Player player, String key) {
        WitherSkeleton entity = (WitherSkeleton) player.getWorld().spawnEntity(player.getLocation(), EntityType.WITHER_SKELETON);
        entity.setSilent(true);
        entity.setAI(true);
        entity.setGravity(false);
        entity.setCanPickupItems(false);
        entity.setCollidable(false);
        entity.setGliding(true);
        entity.addPotionEffect(
                new PotionEffect(
                        PotionEffectType.INVISIBILITY, 1000, 0, true, false
                ), true);
        entity.getEquipment().clear();
        entity.getEquipment().setItemInMainHand(MAIN.manager.getItemStack(key));
        SwordData sword = MAIN.manager.getSword(key);
        Vector vector = player.getLocation().getDirection().normalize().multiply(sword.SPEED);
        entity.setVelocity(vector);
        Location endLoc = player.getTargetBlock(null, sword.FLY_DISTANCE).getLocation();
        double distance = endLoc.distance(player.getLocation());
        stands.put(player.getUniqueId(), new EntityData(entity, key, endLoc, vector.clone(), distance));
        usermap.put(entity.getUniqueId(), player.getUniqueId());
    }
}
